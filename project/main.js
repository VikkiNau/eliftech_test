
const bank = {
    bankName: false,
    insertRate: false,
    maxLoan: false,
    minDownPayment: false,
    loanTerm: false,

};

let form = document.getElementById("form-create");
let show = document.getElementById("mortgage");
let lu = document.createElement("lu");
show.append(lu);
let keys = Object.keys(localStorage);
for (let key of keys) {
    let li = document.createElement("li");
    lu.append(li);
    li.id = `${key}`;
    let showInfoBank = JSON.parse(localStorage.getItem(key));
    li.innerText = showInfoBank.bankName + " " + showInfoBank.insertRate + " " + showInfoBank.maxLoan + " " + showInfoBank.minDownPayment + " " +
        showInfoBank.loanTerm + " ";
    let editButton = document.createElement("button");
    let removeButton = document.createElement("button");
    li.append(editButton);
    li.append(removeButton);
    editButton.innerText = "Edit";
    removeButton.innerText = "Remove";

    editButton.onclick = () => editFunction(key);
    removeButton.onclick = () => removeFunction(key, li);
}

function editFunction(key, li) {
    document.getElementById("create-button").style.display = "none";
    let editInfoBank = JSON.parse(localStorage.getItem(key));
    document.getElementById("bank-name").value = editInfoBank.bankName;
    document.getElementById("insert-rate").value = editInfoBank.insertRate;
    document.getElementById("max-loan").value = editInfoBank.maxLoan;
    document.getElementById("min-down-payment").value = editInfoBank.minDownPayment;
    document.getElementById("loan-term").value = editInfoBank.loanTerm;
    form.style.display = "unset";
    createBank();
    removeFunction(key, li);

}

function removeFunction(key, li) {
    li.remove();
    localStorage.removeItem(key);
}

function showForm() {
    lu.style.display = "none";
    form.style.display = "unset";
    document.getElementById("create-button").style.display = "none";
}

function createBank() {
    let bankMortgage = Object.create(bank);

    bankMortgage.bankName = document.getElementById("bank-name").value;
    bankMortgage.insertRate = document.getElementById("insert-rate").value;
    bankMortgage.maxLoan = document.getElementById("max-loan").value;
    bankMortgage.minDownPayment = document.getElementById("min-down-payment").value;
    bankMortgage.loanTerm = document.getElementById("loan-term").value;

    localStorage.setItem(bankMortgage.bankName, JSON.stringify(bankMortgage));

}



function createMortgage() {
    let bankNameLoan = document.getElementById("bank-name-loan").value;
    let initialLoan = document.getElementById("initial-loan").value;
    let downPayment = document.getElementById("down-payment").value;

    let amountBorrowed = parseInt(initialLoan) - parseInt(downPayment);//P
    let monthlyPaymant;

    let payOfMonth = document.getElementById("pay-of-month");
    let canPayOfMonth = document.createElement("p");
    payOfMonth.append(canPayOfMonth);


    let keys = Object.keys(localStorage);
    for (let key of keys) {
        let loadBank = JSON.parse(localStorage.getItem(key));
        let annualInterestRate = parseInt(loadBank.insertRate);
        let numberOfMonthlyPayments = parseInt(loadBank.loanTerm);
        if (bankNameLoan === key && parseInt(initialLoan) <= parseInt(loadBank.maxLoan) && parseInt(downPayment) >= parseInt(loadBank.minDownPayment)) {
            monthlyPaymant = (amountBorrowed * (annualInterestRate / 12) * ((1 + (annualInterestRate / 12)) ^ numberOfMonthlyPayments)) / (((1 + (annualInterestRate / 12)) ^ numberOfMonthlyPayments) - 1)
            canPayOfMonth.innerText = monthlyPaymant;
        } else {
            canPayOfMonth.innerText = "ERROR";
        }
    }

}